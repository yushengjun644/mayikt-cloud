# mayiktCloud

#### 介绍
 MayiktCloud服务注册中心框架,实现服务注册与发现,服务续约、剔除、心跳检测及注册中心高可用 支持AP和CP模式
 **关注微信公众号 架构师余胜军 回复"88" ，可以直接获取该项目最新源码** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/165814_7cbf257a_926394.png "屏幕截图.png")


#### 设计作者：
 97年程序员、余胜军  我的教学网站www.mayikt.com 百度搜索 蚂蚁课堂  微信yushengjun644 QQ644064065  
官方粉丝交流QQ群：892435455

#### 软件架构
1.  启动MayiktCloud服务注册中心Server端
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/165117_b5f376d3_926394.png "屏幕截图.png")
2.  客户端 引入jar包：

```
 <dependency>
            <artifactId>mayikt-cloud-register-client-spring-boot-starter</artifactId>
            <groupId>com.mayikt</groupId>
            <version>1.0-SNAPSHOT</version>
      </dependency>
```


3.  客户端 或者将mayikt-cloud-register-client-spring-boot-starter插件打入本地仓库中
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/165229_3d220796_926394.png "屏幕截图.png")
4.  客户端配置文件中需要配置如下：

```
mayikt:
  cloud:
    service-name: mayikt-member 
    server-register-url: http://127.0.0.1:8848
server:
  port: 8086

```
5.  客户端启动成功会将该ip和端口注册到注册中心上
 访问：http://127.0.0.1:8848
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/165355_f69147e5_926394.png "屏幕截图.png")

